# smb-services-parked

This is the parked website for [SMB.Services](https://SMB.Services).


## Theme

This Hugo site uses the [Highlights](https://github.com/schmanat/hugo-highlights-theme) theme.

```bash
git submodule add https://github.com/schmanat/hugo-highlights-theme themes/hugo-highlights-theme
```

